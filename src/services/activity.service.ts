/**
 * Created by Tobias Krispinsson on 2017-01-17.
 */

import { Injectable } from '@angular/core';
// import { Headers, Http } from '@angular/http';
import {Http, Headers, RequestOptions, RequestMethod, Request} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Activity} from "../pages/activity/activity";

@Injectable()
export class ActivityService{

  // private headers = new Headers({'Content-Type': 'application/json'});
  private getAllActivitiesURL = "http://localhost:3000/api/getAllactivities/1";
  // private getAllActivitiesURL = "http://localhost:3000/api/getAllActivities/1";
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http){}


  /**
   * Retrieve upcoming activities that a specific user has "joined"
   * @param user: That specific user
   * @returns {({title: string, description: string, img: string}|{title: string, description: string, img: string})[]}
   */
  getUpcomingActivities(user){

    let upcomingActivities = [{
      id: 3,
      title: "Gitarrjam",
      description: "Jag är en tjej som gillar att spela gitarr men jag skulle gärna spela med några fler. Kom till mig och drick " +
      "kaffe och jamma med mig så får vi se vart det leder",
      img: "../../assets/img/guitar_thumb.jpg"
    },
      {
        id: 4,
        title: "Förfest",
        description: "FEST!!",
        img: "../../assets/img/party_thumb.jpg"
      }];

    return upcomingActivities;
  }

  /**
   * Retrieve all activities based on the location of the user
   * @param location: GPS coordinates for the user
   * @returns {({title: string, description: string, location: string, img: string}|{title: string, description: string, location: string, img: string})[]}
   */
  getAllActivities(location): Promise<Activity[]>{

    return this.http.get(this.getAllActivitiesURL)
      .toPromise().then(response => response
        .json() as Activity[])
      .catch(this.handleError);

  }

  addActivity(activity): Promise<Boolean> {

    let url = "http://localhost:3000/api/addActivity";

    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: this.headers,
      body: JSON.stringify(activity)
    });

    return this.http.request(new Request(requestOptions))
      .toPromise()
      .then(() => true)
      .catch(this.handleError);

  }

  joinActivity(id): Promise<Boolean> {

    let url = "http://localhost:3000/api/joinActivity";

    let requestOptions = new RequestOptions({
      method: RequestMethod.Post,
      url: url,
      headers: this.headers,
      body: JSON.stringify(id)
    });

    console.log(JSON.stringify(id));

    return this.http.request(new Request(requestOptions))
      .toPromise()
      .then(() => true)
      .catch(this.handleError);
  }



  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }


}
