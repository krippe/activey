import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';

import { ActivityAdd } from '../pages/activityAdd/activityAdd';
import { Profile } from '../pages/profile/profile';
import { Feed } from '../pages/feed/feed';
import { TabsPage } from '../pages/tabs/tabs';
import { ActivityService } from  '../services/activity.service';
import { Storage } from '@ionic/storage';
import { ActivityComponent } from '../pages/activity/activity.component';

@NgModule({
  declarations: [
    MyApp,
    ActivityAdd,
    Profile,
    Feed,
    TabsPage,
    ActivityComponent
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ActivityAdd,
    Profile,
    Feed,
    TabsPage,
    ActivityComponent
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, ActivityService, Storage]
})
export class AppModule {}
