import {Component, OnInit} from '@angular/core';

import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import {ActivityService} from "../../services/activity.service";


@Component({
  selector: 'page-profile',
  templateUrl: 'profile_template.html'
})
export class Profile implements OnInit {

  number;
  title;
  upcomingActivities;

  constructor(public navCtrl: NavController, private activityService: ActivityService, private storage: Storage) {
  }
    ngOnInit() {

      this.upcomingActivities = this.activityService.getUpcomingActivities(1);
    //   this.storage.get("title").then((title) => {
    //     console.log(title);
    //     this.title = title;
    //   });
    }
}
