import {Component, OnInit} from '@angular/core';

import { NavController } from 'ionic-angular';

import {ActivityService} from "../../services/activity.service";
import {ActivityComponent} from "../../pages/activity/activity.component"
import {Activity} from "../activity/activity";

@Component({
  selector: 'page-feed',
  templateUrl: 'feed_template.html'
})
export class Feed implements OnInit{

  activities: Activity[];
  // activities;

  constructor(
    public navCtrl: NavController,
    private activityService: ActivityService) {
  }

  ngOnInit() {

    this.getAllActivities();
  }


  getAllActivities(): any {

    this.activityService.getAllActivities(1).then(activities => this.activities = activities);

  }


  openActivity(activity){
    // console.log(activity);
    this.navCtrl.push(ActivityComponent, {info: activity});
  }

}
