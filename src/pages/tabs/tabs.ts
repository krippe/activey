import { Component } from '@angular/core';

import { Feed } from '../feed/feed';
import { ActivityAdd } from '../activityAdd/activityAdd';
import { Profile } from '../profile/profile';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = Feed;
  tab2Root: any = ActivityAdd;
  tab3Root: any = Profile;

  constructor() {

  }
}
