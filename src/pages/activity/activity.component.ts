/**
 * Created by Tobias Krispinsson on 2017-01-17.
 */

import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {ActivityService} from "../../services/activity.service";

@Component({
  selector: 'page-activity',
  templateUrl: 'activity_template.html'
})
export class ActivityComponent {

  info;

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private activityService: ActivityService,
              private toastCtrl: ToastController) {}

  ngOnInit() {
    this.info = this.navParams.get('info');
    console.log(this.info);
  }

  joinActivity(id): void {
    console.log("activityComponent" + id);

    var that = this;

    this.activityService.joinActivity(id)
      .then(function (res) {
        that.joinSuccessToast();
      });
  }

  joinSuccessToast() {
    let toast = this.toastCtrl.create({
      message: 'Du har nu gått med i denna aktivitet',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
