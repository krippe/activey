/**
 * Created by Tobias Krispinsson on 2017-01-23.
 */

export class Activity {

  id: number;
  title: string;
  description: string;
  location: string;
  category: string;
  // img: string;
  time: string;
  date: string;
  min_participants: number;
  max_participants: number;
  boys: string;
  girls: string;
  alcohol: boolean;
  niceTime: string;
  niceDate: string;
  // numberOfPeople: number;
  // placesLeft: number;

}
