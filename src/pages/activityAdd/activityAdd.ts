import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

import {NavController, ToastController} from 'ionic-angular';

import { ActivityService } from  '../../services/activity.service';

@Component({
  selector: 'page-activityAdd',
  templateUrl: 'activityAdd_template.html'
})
export class ActivityAdd {

  constructor(public navCtrl: NavController, private storage: Storage, private activityService: ActivityService, private toastCtrl: ToastController) {
  }

  activity = {};


  logForm(): void {

    var that = this;

    this.activityService.addActivity(this.activity)
      .then(function (res) {
      console.log("KOMMER JAG HIT I activityAdd");
       that.successToast();
      });
  }

  successToast() {
    let toast = this.toastCtrl.create({
      message: 'User was added successfully',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
